import React from 'react'
import Counts from './Counts'
import { useSelector } from 'react-redux'


const Dashboard = () => {

  const posts = useSelector((state) => state.postdata) //fetching posts of users
  const userdata = useSelector((state) => state.getuserdata) //fetching user info
  const comments = useSelector((state) => state.commentsdata) //fetching comments
  const photodata = useSelector((state) => state.photodata) //fetching photos
  const albumdata = useSelector((state) => state.albumdata) //fetching albums
  const tododata = useSelector((state) => state.tododata) //fetching todos

  
  return (
    <>
      <Counts userdata={userdata} posts={posts} comments={comments} photodata={photodata} albumdata={albumdata} tododata={tododata} />
    </>
  )
}

export default Dashboard