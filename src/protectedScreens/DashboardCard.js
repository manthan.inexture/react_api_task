import React from 'react'
import { Link} from "react-router-dom";
const DashboardCard = (props) => {
  return (
      <div className="col-md-4 py-1">
          <div className={`card border-2 border-${props.styletype}`}>
              <div className="card-body">
                  <div className={`card-title text-${props.styletype}`}> <h3>{props.length}</h3></div><br /><br />
                  <h6 className="card-subtitle mb-2 text-muted ">{props.name}</h6>
                  <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  <Link to={props.path} className={`small-box-footer text-${props.styletype}`}>
                      More info <i className="fas fa-arrow-circle-right"></i>
                  </Link>
              </div>
          </div>
      </div>
  )
}

export default DashboardCard