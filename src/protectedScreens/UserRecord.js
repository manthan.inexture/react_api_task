import React, { useEffect, useState } from 'react'
import {Link} from "react-router-dom";
import { useSelector } from 'react-redux'
import UserRecordmodel from './UserRecordmodel';

const UserRecord = (props) => {

    let userData = useSelector((state) => state.getuserdata) //fetching user info
    const [searchbox, setsearchbox] = useState("close")
    const [result, setresult] = useState([])
    const [filter, setFilter] = useState([]);
    const [sortBy, setSortBy] = useState(null);


    const handleserch = (e) => {
        if ((e.target.value).length >= 3) {
            setresult(userData.filter((data) => {
                return ((data.name).toLowerCase()).includes((e.target.value).toLowerCase())
            }))
            setsearchbox("open")
        } else {
            setsearchbox("close")
        }
    }

    useEffect(() => {
        setFilter(userData)
    }, [userData])

    useEffect(() => {
        sortBy && Handlesort(sortBy)
    }, [sortBy])

    const Handlesort = (sort) => {
        const data = [...userData]
         data.sort(function (a, b) {
            if (sort.target.value === "name") {
                var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase();
            }
            else if (sort.target.value === "number") {
                nameA = a.id
                nameB = b.id;
            }
            return (nameA < nameB) ? -1 : ((nameA > nameB) ? 1 : 0)
        });
        setFilter(data);
    }
    return (
            <div className="container">
              <div className="row py-4">
                <div className="col-md-8 offset-md-1">
                    <div className={`form-inline sidebar-search-${searchbox}`}>
                        <div className="input-group w-100 input-group-lg" data-widget="sidebar-search">
                            <input className="form-control  form-control-lg " onChange={(e) => handleserch(e)} type="search" placeholder="Search" aria-label="Search" />
                            <div className="input-group-append">
                                <button type="submit" className="btn btn-lg btn-success">
                                    <i className="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <div className="sidebar-search-results">
                            <div className="list-group">
                                {
                                    result.length ? result.map((user) => (
                                        <Link to={`/userpost/${user.id}`} className="list-group-item">
                                            <div className="search-title">{user.name}</div>
                                        </Link>
                                    ))
                                        : <a href="#" className="list-group-item">
                                            <div className="search-title">No result found</div>
                                        </a>
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className=" col-md-2 dropdown ml-2">
                    <select className="form-select btn-info  form-select-lg mb-3" onChange={(e) => setSortBy(e)} aria-label=".form-select-lg example">
                        <option >Short By</option>
                        <option value="name">Name(A-Z)</option>
                        <option value="number">Number(1-N)</option>
                    </select>
                </div>
            </div>
            <table className="table  rounded-3 table-hover border-3">
                <thead>
                    <tr className='table-light'>
                        <th scope="col" onClick={() => setSortBy({ target: { value: "number" } })}>#</th>
                        <th scope="col" onClick={() => setSortBy({ target: { value: "name" } })}>Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">More </th>
                        <th scope="col"> View Post</th>
                        <th scope="col"> View Todos</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        filter?.length > 0 ? filter.map((element, index) => (
                            <tr key={index} className='table-hover table-light'>
                                <th scope="row" >{element.id}</th>
                                <td>{element.name}</td>
                                <td>{(element.email).toLowerCase()}</td>
                                <td className='text'>
                                    <button type="button" className="btn btn-warning" data-bs-toggle="modal" data-bs-target={`#data${element.id}`}>
                                        More
                                    </button>
                                </td>
                                <td>
                                    <Link to={`/userpost/${element.id}`} className="btn btn-primary">
                                        view post
                                    </Link>
                                </td>
                                <td>
                                    <Link to={`/todos/${element.id}`} className="btn btn-danger">
                                        view todo
                                    </Link>
                                </td>
                                <UserRecordmodel element={element} />
                            </tr>
                        )) : <div className="spinner-grow text-primary" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    }
                </tbody>
            </table>

        </div> 
        
    )
}

export default UserRecord