import React from 'react'

const UserRecordmodel = (props) => {
  return (
  <div className="modal fade" id={`data${props.element.id}`} tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-dialog-scrollable">
              <div className="modal-content">
                  <div className="modal-header">
                      <h5 className="modal-title" id="exampleModalLabel">{props.element.name}</h5>
                      <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div className="modal-body">
                      <div className="row">
                          <div className="col-md-6" >
                              <div className="mb-3">
                                  <label className="form-label">Name </label>
                                  <input type="text" className="form-control" value={props.element.name} disabled />
                              </div>
                          </div>
                          <div className="col-md-6" >
                              <div className="mb-3">
                                  <label className="form-label">Username</label>
                                  <input type="text" className="form-control" value={props.element.username} disabled />
                              </div>
                          </div>
                          <div className="col-md-12" >
                              <div className="mb-3">
                                  <label className="form-label">Email address</label>
                                  <input type="text" className="form-control" value={(props.element.email).toLowerCase()} disabled />
                              </div>
                              <div className="mb-3">
                                  <label className="form-label">Phone</label>
                                  <input type="text" className="form-control" id="exampleInputPassword1" value={props.element.phone} disabled />
                              </div>
                          </div>
                          <label className="form-label"> Address</label>
                          <div className="col-md-6" >
                              <div className="mb-3">
                                  <label className="form-label">Street</label>
                                  <input type="text" className="form-control" value={props.element.address.street} disabled />
                              </div>
                          </div>
                          <div className="col-md-6" >
                              <div className="mb-3">
                                  <label className="form-label">suite</label>
                                  <input type="text" className="form-control" id="exampleInputPassword1" value={props.element.address.suite} disabled />
                              </div>
                          </div>
                          <div className="col-md-6" >
                              <div className="mb-3">
                                  <label className="form-label">city</label>
                                  <input type="text" className="form-control" value={props.element.address.city} disabled />
                              </div>
                          </div>
                          <div className="col-md-6">
                              <div className="mb-3">
                                  <label className="form-label">Zipcode</label>
                                  <input type="text" className="form-control" id="exampleInputPassword1" value={props.element.address.zipcode} disabled />
                              </div>
                          </div>
                          <div className="col-md-12" >
                              <label className="form-label" >Company</label>
                              <div className="mb-3">
                                  <label className="form-label">Company name</label>
                                  <input type="email" className="form-control" id="exampleInputEmail1" value={props.element.company.name} aria-describedby="emailHelp" disabled />
                              </div>
                              <div className="mb-3">
                                  <label className="form-label">catchPhrase</label>
                                  <input type="text" className="form-control" id="exampleInputPassword1" value={props.element.company.catchPhrase} disabled />
                              </div> <div className="mb-3">
                                  <label className="form-label">bs</label>
                                  <input type="text" className="form-control" id="exampleInputPassword1" value={props.element.company.bs} disabled />
                              </div>
                          </div>
                      </div>
                  </div>
                  <div className="modal-footer">
                      <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  </div>
              </div>
          </div>
      </div>
  )
}

export default UserRecordmodel