import React from 'react'
import { Outlet ,Navigate} from 'react-router-dom'

const PrivateRoutes = () => {
  const login = localStorage.getItem("login")
    return (
  <>
      {
          login === "true"?
              <Outlet /> : <Navigate to="/login" />
            }
        </>
  )
}

export default PrivateRoutes