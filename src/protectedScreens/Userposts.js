import React, { useEffect } from 'react'
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux'
import { fetchrecords } from '../redux/user/userAction'
import { fetchposts } from '../redux/posts/postActions'
const Userposts = () => {
  const { id } = useParams()
  // console.log(id);
  const posts = useSelector((state) => state.postdata) //fetching posts of users
  const userdata = useSelector((state) => state.getuserdata) //fetching user info

  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchrecords())
    dispatch(fetchposts())
  }, [])

  return (
    <div className="container pt-4">
      <div className="row">
        {userdata.length ? userdata.map((user, index) => {
          if (id == user.id) {
            return (
              <div className='col-md-4' key={index}>
                <div className="mycard">
                  <div className="card p-4">
                    <div className=" image d-flex flex-column justify-content-center align-items-center">
                      <button className="btn btn-secondary">
                        <img src="https://i.imgur.com/wvxPV9S.png" height="100" width="100" />
                      </button>
                      <span className="name mt-3"> {`${user.name}`}</span>
                      <span className="idd">@{`${user.username}`}</span>
                      <div className="d-flex flex-row justify-content-center align-items-center gap-2">
                        <span className="idd1">Oxc4c16a645_b21a</span>
                        <span><i className="fa fa-copy"></i></span> </div>
                      <div className="d-flex flex-row justify-content-center align-items-center mt-3">
                        <span className="number">1069 <span className="follow">Followers</span></span> </div>
                      <div className=" d-flex mt-2"> <button className="btn1 btn-dark">Edit Profile</button> </div>
                      <div className="text mt-3">
                        <span>Eleanor Pena is a creator of minimalistic x bold graphics and digital artwork.<br /><br />
                          Artist/ Creative Director by Day #NFT minting@ with FND night. </span>
                      </div>
                      <div className="gap-3 mt-3 icons d-flex flex-row justify-content-center align-items-center">
                        <span><i className="fa fa-twitter"></i></span>
                        <span><i className="fa fa-facebook-f"></i></span>
                        <span><i className="fa fa-instagram"></i></span>
                        <span><i className="fa fa-linkedin"></i></span> </div>
                      <div className=" px-2 rounded mt-4 date "> <span className="join">Joined May,2021</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>)
          }
        }) : ""}
        <div className='col-md-8'>
          <h2 className="py-3 text-center bg-dark">Posts</h2>  {
            posts.length ? posts.map((posts, index) => {
              if (id == posts.userId) {
                return (
                  <div key={index} className="accordion accordion-flush" id="accordionFlushExample">
                    <div className="accordion-item">
                      <h2 className="accordion-header border" id="flush-headingOne">
                        <button className="accordion-button collapsed border-dark " type="button" data-bs-toggle="collapse" aria-expanded="true" data-bs-target={`#post${posts.id}`} aria-controls="flush-collapseOne">
                          {posts.title}
                        </button>
                      </h2>
                      <div id={`post${posts.id}`} className="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">{posts.body}</div>
                      </div>
                    </div>
                  </div>
                )
              }
            }) : <h3>loading .....</h3>
          }</div>
      </div>
    </div>
  )
}

export default Userposts