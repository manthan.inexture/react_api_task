import React from 'react'
import { useSelector } from 'react-redux'
import { useParams } from "react-router-dom";
const Photos = () => {
  const photodata = useSelector((state) => state.photodata) //fetching comments
  const { id } = useParams()
  return (
    <div className="container">
      <div className="row">
        {
          photodata.length ? photodata.map((photo,index) => {
            if (id == photo.albumId) {
              return (
                <div key={index} className="col-md-3 py-3">
                <div className="card bg-dark text-white">
                  <img src={photo.url} className="card-img" alt="..." />
                  <div className="card-img-overlay">
                      <h5 className="card-title">{photo.title}</h5>
                  </div>
                  </div>
                </div>
              )

            }
          }) : <h3>loging......</h3>

          }
      </div>
      
    </div>
  )
}

export default Photos