import React, { useRef} from 'react'
import { useParams } from "react-router-dom";
import { useSelector } from 'react-redux'
const Todolist = () => {
    const { id } = useParams()
    const inputEl = useRef(null);
    const todos = useSelector((state) => state.tododata) //fetching posts of users
    console.log(inputEl);
    return (
        <div><div ref={inputEl} className="card container mt-4">
            <div className="card-header ui-sortable-handle">
                <h3 className="card-title">
                    <i className="ion ion-clipboard mr-1"></i>
                    To Do List
                </h3> 
            </div>
            <div className="card-body">
                <ul className="todo-list ui-sortable" data-widget="todo-list">
                    {
                        todos.length ? todos.map((todo,index) => {
                            if (id === todo.userId) {
                                return (
                                    <li key={index} className={todo.completed ? "done" : ""}>
                                    <span className="handle ui-sortable-handle">
                                        <i className="fas fa-ellipsis-v"></i>
                                        <i className="fas fa-ellipsis-v"></i>
                                    </span>
                                    <div className="icheck-primary d-inline ml-2">
                                        <input type="checkbox" value="" name="todo1" id="todoCheck1" />
                                          <label for="todoCheck1"></label>
                                    </div>
                                    <span className="text">{ todo.title}</span>
                                    <small className="badge badge-success"><i className="far fa-clock"></i> 2 mins</small>
                                    <div className="tools">
                                        <i className="fas fa-edit"></i>
                                        <i className="fas fa-trash-o"></i>
                                    </div>
                                </li>)
                            }
                        }) : <h1>loding.........</h1>
}
                </ul>
            </div>
            <div className="card-footer clearfix">
                <button type="button" className="btn btn-primary float-right"><i className="fas fa-plus"></i> Add item</button>
            </div>
        </div></div>
    )
}

export default Todolist