import React from 'react'
import { useSelector } from 'react-redux'
import {Link} from 'react-router-dom'
const Albums = () => {
    const albumdata = useSelector((state) => state.albumdata) //fetching comments
  return (
      <div className="container pt-4">
          <table className="table">
              <thead>
                  <tr>
                      <th scope="col">#</th>
                      <th scope="col">Album Name</th>
                      <th scope="col">Go</th>
                  </tr>
              </thead>
              <tbody>
                  {
                      albumdata.length ? albumdata.map((album,index) => {
                          return (<tr key={index}>
                              <th scope="row">{album.id }</th>
                              <td>{album.title}</td>
                              <td><Link className="btn btn-primary"to={`photos/${album.id}`} >GO</Link></td>
                          </tr>)
                      }) : <h3>loging ....</h3>
                  }
              </tbody>
          </table>
          </div>
  )
}

export default Albums