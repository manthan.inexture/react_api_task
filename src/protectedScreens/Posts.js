import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import InfiniteScroll from 'react-infinite-scroll-component';
const Posts = () => {
    const [loadmore, setLoadmore] = useState(10);
    const posts = useSelector((state) => state.postdata) //fetching posts of users
    const comments = useSelector((state) => state.commentsdata) //fetching comments
console.log(loadmore);
    return (
        <div className="container pt-4">
            <h3 className='text-center'>Posts</h3>
            <div className="accordion" id="accordionExample">
                    <InfiniteScroll
                        dataLength={posts.length}
                        next={ () => setLoadmore(loadmore + 10)}
                    style={{ display: 'flex',overflow: 'hidden' }} //To put endMessage and loader to the top.
                        hasMore={true}
                        scrollableTarget="scrollableDiv"
                    >   
                <div className='row'>
                        {
                            posts.length ? posts.slice(0, loadmore).map((post,index) => {
                                return (
                                    <>
                                    <div key={index} className="col-md-6 my-2">
                                            <div className="card card-group  h-100 my-2">
                                                <div className="accordion-item " >
                                                <div className="card card-widget m-0 h-100">
                                                    <div className="card-header bg-dark">   
                                                        <div className="user-block">
                                                            <img className="img-circle" src="https://www.bootdey.com/img/Content/avatar/avatar3.png" alt="User Image" />
                                                            <span className="username text-light">{post.title}</span>
                                                            <span className="description">Shared publicly - 7:30 PM Today</span>
                                                        </div>
                                                        {/* /.user-block */}
                                                    </div>
                                                    {/* /.card-header */}
                                                    <div className="card-body">
                                                        {/* post text */}
                                                        <p>{post.body}</p>
                                                        <button type="button" className="btn btn-default btn-sm"><i className="fas fa-share" /> Share</button>
                                                        <button type="button" className="btn btn-default btn-sm"><i className="far fa-thumbs-up" /> Like</button>
                                                        <button type="button" className="btn btn-default btn-sm" data-bs-toggle="collapse" data-bs-target={`#btn${post.id}`} aria-expanded="false" aria-controls={`btn${post.id}`}><i className="far fa-comment" /> Comments</button>
                                                        <span className="float-right text-muted">45 likes - 2 comments</span>
                                                    </div>
                                                    {/* /.card-body */}
                                                    {
                                                        comments.map((comments,index) => {
                                                            if (post.id == comments.postId) {
                                                                return (
                                                                    <div key={index} id={`btn${post.id}`} className="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                                        <div className="card-footer card-comments">
                                                                            <div className="card-comment">
                                                                                {/* User image */}
                                                                                <img className="img-circle img-sm" src="https://www.bootdey.com/img/Content/avatar/avatar3.png" alt="User Image" />
                                                                                <div className="comment-text">
                                                                                    <span className="username">
                                                                                        {comments.name.split(" ").slice(0, 2)}
                                                                                        <span className="text-muted float-right">8:03 PM Today</span>
                                                                                    </span>{/* /.username */}
                                                                                    {comments.body}
                                                                                </div>
                                                                                {/* /.comment-text */}
                                                                            </div>
                                                                            {/* /.card-comment */}
                                                                        </div>
                                                                        <div className='py-1'></div>
                                                                    </div>)
                                                            }
                                                        })
                                                    }
                                                    {/* /.card-footer */}
                                                    <div className="card-footer mb-0 ">
                                                        <form action="#" method="post">
                                                            <i className="fa-solid fs-2 fa-circle-arrow-right img-fluid img-circle img-sm"></i>
                                                            {/* <img className="img-fluid img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="Alt Text" /> */}
                                                            {/* .img-push is used to add margin to elements next to floating images */}
                                                            <div className="img-push">
                                                                <input type="text" className="form-control form-control-sm" placeholder="Press enter to post comment" />
                                                            </div>
                                                        </form>
                                                    </div>
                                                    {/* /.card-footer */}
                                                </div>
                                            </div>
                                            {/*/.direct-chat */}
                                        </div>
                                        </div>
                                    </>
                                )
                            }) : <h2>loding .....</h2>
                        }
                </div>
                    </InfiniteScroll>
            </div>
            <button className="btn btn-success" onClick={() => setLoadmore(loadmore + 10)}>Loadmore</button>
        </div>
    )
}

export default Posts