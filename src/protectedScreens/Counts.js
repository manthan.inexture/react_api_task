import React from 'react'
import { useSelector } from 'react-redux'
import DashboardCard from './DashboardCard';

const Counts = () => {
  
  const posts = useSelector((state) => state.postdata) //fetching posts of users
  const userdata = useSelector((state) => state.getuserdata) //fetching user info
  const comments = useSelector((state) => state.commentsdata) //fetching comments
  const photodata = useSelector((state) => state.photodata) //fetching photos
  const albumdata = useSelector((state) => state.albumdata) //fetching albums


  return (
    <div className="container p-4">
      <div className="row">
        <DashboardCard name={"User"} path={"/users"} length={userdata.length} styletype={"warning"} />  
        <DashboardCard name={"Posts"} path={"/posts"} length={posts.length} styletype={"success"} />
        <DashboardCard name={"Comments"} path={"/posts"} length={comments.length} styletype={"danger"} />
        <DashboardCard name={"Albums"} path={"/albums"} length={albumdata.length} styletype={"info"} />
        <DashboardCard name={"Photos"} path={"/albums"} length={photodata.length} styletype={"primary"} />
      </div>
    </div>
  )
}

export default Counts