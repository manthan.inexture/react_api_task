import React, { useState } from 'react'
import {
    Routes, Route, Link, useNavigate, Navigate,
    useLocation
} from "react-router-dom";

const Header = () => {
    const navigate = useNavigate();
    let login = localStorage.getItem("login");
    const logout = (e) => {
        e.preventDefault();
        localStorage.setItem("login", false)
        navigate('/login');
    }
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container-fluid">
                <Link className="navbar-brand" to="/">React Task</Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            {
                                login == "true" ?
                                <Link className="nav-link active" aria-current="page" to='/'>Dashboard</Link> : ""}
                        </li>
                        <li className="nav-item">
                            {
                                login !== "true" ?
                                <Link className="nav-link" to='/login'>Login</Link> : ""
                            }
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to='/signup'>Signup</Link>
                        </li>
                    </ul>
                    <form className="d-flex">
                        {
                            login == "true" ?
                                <button className="btn btn-success" onClick={(e) => { logout(e) }}>Logout</button> : ""
                        }
                    </form>
                </div>
            </div>
        </nav>
    )
}

export default Header