import React from 'react'
import { useSelector } from 'react-redux'
const useSort = () => {
    // console.log(sort);
    const userData = useSelector((state) => state.getuserdata) //fetching user info

    return async(sort) => {
       return   await  userData.sort(function (a, b) {
            if (sort == "name") {
                var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase();
            }
            else if (sort == "number") {
                var nameA = a.id, nameB = b.id;
            }
            if (nameA < nameB) //sort string ascending
                return -1;
            if (nameA > nameB)
                return 1;
            return 0; //default return value (no sorting)
        });
    }
//     console.log(userData);
//   return [userData]
}

export default useSort