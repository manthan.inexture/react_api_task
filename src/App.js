import {useEffect} from 'react'
import Router from './Router';
import { useDispatch } from 'react-redux'
import { fetchrecords } from './redux/user/userAction'
import { fetchposts } from './redux/posts/postActions'
import { fetchcomments } from './redux/comments/commentAction'
import { fetchalbums } from './redux/album/albumAction'
import { fetchphotos } from './redux/photos/photoAction'
import { fetchtodos } from './redux/todos/todoAction'
function App() {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchrecords())
    dispatch(fetchposts())
    dispatch(fetchcomments())
    dispatch(fetchalbums())
    dispatch(fetchphotos())
    dispatch(fetchtodos())
  }, [])
  return (
  <Router />
  );
}

export default App;
