import { FETCHPOST } from './postTypes'
const initalstate = []

const postdata = (state = initalstate, action) => {
    switch (action.type) {
        case FETCHPOST:
            return action.payload
        default:
            return state
    }
}
export { postdata };