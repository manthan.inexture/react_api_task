import { FETCHPOST } from './postTypes'
import axios from 'axios'
export const fetchposts = () => {
    return (dispatch) => {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then((response) => {
                dispatch({
                    type: FETCHPOST,
                    payload: response.data
                })
            })
            .catch((error) => {
                dispatch({
                    type: FETCHPOST,
                    payload: error
                })
            })
    }
}