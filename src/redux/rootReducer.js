import { combineReducers } from "redux"
import { getuserdata } from './user/userReducer'
import { postdata } from './posts/postReducer'
import { commentsdata } from './comments/commentReducer'
import { albumdata } from './album/albumReducer'
import { photodata } from './photos/photoReducer'
import { tododata } from "./todos/todoReducer"

const rootreducer = combineReducers({ getuserdata, postdata, commentsdata, albumdata, photodata,tododata })

export default rootreducer