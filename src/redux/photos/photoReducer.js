import { FETCHPHOTO } from './photoTypes'
const initalstate = []

const photodata = (state = initalstate, action) => {
    switch (action.type) {
        case FETCHPHOTO:
            return action.payload
        default:
            return state
    }
}
export { photodata };