import { FETCHPHOTO } from './photoTypes'

import axios from 'axios'
export const fetchphotos = () => {
    return (dispatch) => {
        axios.get('https://jsonplaceholder.typicode.com/photos')
            .then((response) => {
                dispatch({
                    type: FETCHPHOTO,
                    payload: response.data
                })
            })
            .catch((error) => {
                dispatch({
                    type: FETCHPHOTO,
                    payload: error
                })
            })
    }
}