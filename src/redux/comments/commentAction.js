import { FETCHCOMMENT } from './commentTypes'

import axios from 'axios'
export const fetchcomments = () => {
    return (dispatch) => {
        axios.get('https://jsonplaceholder.typicode.com/comments')
            .then((response) => {
                dispatch({
                    type: FETCHCOMMENT,
                    payload: response.data
                })
            })
            .catch((error) => {
                dispatch({
                    type: FETCHCOMMENT,
                    payload: error
                })
            })
    }
}