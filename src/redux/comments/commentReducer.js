import { FETCHCOMMENT } from './commentTypes'
const initalstate = []

const commentsdata = (state = initalstate, action) => {
    switch (action.type) {
        case FETCHCOMMENT:
            return action.payload
        default:
            return state
    }
}
export { commentsdata };