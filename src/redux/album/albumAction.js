import { FETCHALBUM } from './albumTypes'

import axios from 'axios'
export const fetchalbums = () => {
    return (dispatch) => {
        axios.get('https://jsonplaceholder.typicode.com/albums')
            .then((response) => {
                dispatch({
                    type: FETCHALBUM,
                    payload: response.data
                })
            })
            .catch((error) => {
                dispatch({
                    type: FETCHALBUM,
                    payload: error
                })
            })
    }
}