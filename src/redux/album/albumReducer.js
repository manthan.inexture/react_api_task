import { FETCHALBUM } from './albumTypes'
const initalstate = []

const albumdata = (state = initalstate, action) => {
    switch (action.type) {
        case FETCHALBUM:
            return action.payload
        default:
            return state
    }
}
export { albumdata };