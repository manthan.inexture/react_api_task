import { FETCHTODO } from './todoTypes'
import axios from 'axios'
export const fetchtodos = () => {
    return (dispatch) => {
        axios.get('https://jsonplaceholder.typicode.com/todos')
            .then((response) => {
                dispatch({
                    type: FETCHTODO,
                    payload: response.data
                })
            })
            .catch((error) => {
                dispatch({
                    type: FETCHTODO,
                    payload: error
                })
            })
    }
}