import { FETCHTODO } from './todoTypes'
const initalstate = []

const tododata = (state = initalstate, action) => {
    switch (action.type) {
        case FETCHTODO:
            return action.payload
        default:
            return state
    }
}
export { tododata };