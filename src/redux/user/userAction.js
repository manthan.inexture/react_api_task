import { FETCHUSER } from './userTypes'
import axios from 'axios'
export const fetchrecords = () => {
    return (dispatch) => {
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then((response) => {
                dispatch ({
                    type: FETCHUSER,
                    payload: response.data
                })
            })
            .catch((error) => {
                dispatch( {
                    type: FETCHUSER,
                    payload: error
                })
            })
    }
}