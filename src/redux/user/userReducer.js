import {FETCHUSER} from './userTypes'
const initalstate = []

const getuserdata = (state = initalstate, action) => {
    switch (action.type) {
        case FETCHUSER: 
            return action.payload
        default:
            return state
    }
}
export { getuserdata };