import React,{ useState} from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik';
import {useNavigate}from 'react-router-dom'
import * as yup from 'yup';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Login = () => {
    // toast.info(`]]added to cart`, {
    //     position: "bottom-right",
    //     autoClose: 2000,
    //     closeButton: false,
    // });

    const [error, setError] = useState(); // used to show credential error
    const navigate = useNavigate();


    const initVal = { //use formik
        email: "", password: "",
    }
    let submitHandleError = yup.object().shape({ //use of yup for validation
        email: yup.string().required("Email is required").email("email is not valid "),
        password: yup.string().required('Password is required'),
    })

    const handlesubmit = (data) => { // use formik
        const user = JSON.parse(localStorage.getItem("user"))

        if (user.email === data.email && user.password === data.password) { //checking login
            toast('🦄 Wow so easy!', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            alert('login success');
            localStorage.setItem("login", true)  
            setError(null);
            navigate('/');
        }
        else {
            setError('invalid email id & password')
            localStorage.setItem("login", false)
        }

    }
    return (
        <>
            <div className='container'>
            <h1 className='text-center'>Login</h1>
                <div className='row'>
                    <div className='col-md-12 mt-4'  >
                        <Formik initialValues={initVal}
                            validationSchema={submitHandleError}
                            onSubmit={handlesubmit}>
                            <Form>
                                <div className="form-group">
                                    <label>Email:</label>
                                    <Field className="form-control" name="email" type="email" />
                                    <p className='text-danger'>
                                        <ErrorMessage className='text-danger' name="email" />
                                    </p>
                                </div>
                                <div className='row'>
                                    <div className='col-md-12'>
                                        <div className="form-group">
                                            <label>Password:</label>
                                            <Field className="form-control" name="password" type="password" />
                                            <p className='text-danger'>
                                                <ErrorMessage className='text-danger' name="password" />
                                            </p>
                                            <p className='text-danger'>
                                                { error }
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <Field className="btn btn-success" name="submit" type="submit" />
                            </Form>
                        </Formik>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Login