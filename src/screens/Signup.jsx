import React from 'react'
import { Formik, Form, Field, FieldArray, ErrorMessage } from 'formik';
import * as yup from 'yup';
const Signup = () => {
    const initVal = {
        email: "", password: "", cpassword: ""
    }
    
    let submitHandleError = yup.object().shape({
        email: yup.string().required("Email is required").email("Not valid input"),
        password: yup.string().required('Password is required'),
        cpassword: yup.string().oneOf([yup.ref('password'), null], 'Passwords must match'),
    })
    const handlesubmit = (v) => {
        alert(' Dara Submitted Success');
        const userdata = JSON.stringify(v)
        localStorage.setItem("user", userdata)
        console.log(v);
    }
    return (
        <>
            <div className='container'>
                <h1 className='text-center'>Signup</h1>
                <div className='row'>
                    <div className='col-md-12 mt-4'  >
                        <Formik initialValues={initVal}
                            validationSchema={submitHandleError}
                            onSubmit={handlesubmit}>
                            <Form>
                                <div className="form-group">
                                    <label>Email:</label>
                                    <Field className="form-control" name="email" type="email" />
                                    <p className='text-danger'>
                                        <ErrorMessage className='text-danger' name="email" />
                                    </p>
                                </div>
                                <div className='row'>
                                    <div className='col-md-6'>
                                        <div className="form-group">
                                            <label>Password:</label>
                                            <Field className="form-control" name="password" type="password" />
                                            <p className='text-danger'>
                                                <ErrorMessage className='text-danger' name="password" />
                                            </p>
                                        </div>
                                    </div>
                                    <div className='col-md-6'>
                                        <div className="form-group">
                                            <label>Confirm  Password:</label>
                                            <Field className="form-control" name="cpassword" type="password" />
                                            <p className='text-danger'>
                                                <ErrorMessage className='text-danger' name="cpassword" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <Field className="btn btn-success" name="submit" type="submit" />
                            </Form>
                        </Formik>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Signup