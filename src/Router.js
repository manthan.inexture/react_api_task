import Header from './component/Header'
import {
    Routes, Route, Link, useNavigate, Navigate,
    useLocation
} from "react-router-dom";
import Login from './screens/Login'
import Signup from './screens/Signup';
import Dashboard from './protectedScreens/Dashboard'
import PrivateRoutes from './protectedScreens/PrivateRoutes'
import Userposts from './protectedScreens/Userposts';
import UserRecord from './protectedScreens/UserRecord';
import Posts from './protectedScreens/Posts';
import Albums from './protectedScreens/Albums';
import Photos from './protectedScreens/Photos';
import Todolist from './protectedScreens/Todolist';

function Router() {
    return (
        <>
            <Header />
            <Routes>
                <Route path="/" element={<PrivateRoutes />}>
                    <Route path="/" element={<Dashboard />} />
                    <Route path='/users' element={<UserRecord />} />
                    <Route path='/posts' element={<Posts />} />
                    <Route path='albums/photos/:id' element={<Photos />} />
                    <Route path='/albums' element={<Albums />} />
                    <Route path="/userpost/:id" element={<Userposts />} />
                    <Route path="/todos/:id" element={<Todolist />} />
                </Route>
                <Route path="/login" element={<Login />} />
                <Route path="/signup" element={<Signup />} />
                <Route path="*" element={<h1>404 page not found</h1>} />
            </Routes>
        </>
    );
}

export default Router;
